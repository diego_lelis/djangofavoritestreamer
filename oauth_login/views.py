from django.contrib.auth import logout
from django.shortcuts import redirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views import View
from social_core.exceptions import AuthUnknownError


class LogoutView(LoginRequiredMixin, View):
    def get(self, request):
        user = request.user
        try:
            twitch_login = user.social_auth.get(provider='twitch')
            # print(twitch_login.extra_data)
            twitch_login.disconnect(twitch_login)
        except AuthUnknownError:
            print('No social user associated with the user')
        logout(request)
        return redirect('oauth_login:login')
