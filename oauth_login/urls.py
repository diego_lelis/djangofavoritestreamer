from django.conf.urls import url
from django.conf.urls import include
from django.contrib.auth import views as helper_views
from . import views

app_name = 'oauth_login'
urlpatterns = [
    url(r'^login/$', helper_views.login,
        {'template_name': 'oauth_login/login.html'},
        name='login'),
    url(r'^logout/$', views.LogoutView.as_view(), name='logout'),
    url(r'^oauth/', include('social_django.urls', namespace='social')),
]
