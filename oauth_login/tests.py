from django.contrib.auth.models import User
from django.test import TestCase
from social_django.models import UserSocialAuth
from decouple import config
import json


class LogInTestTwitch(TestCase):
    def setUp(self):
        self.credentials = {
            'username': config('TWITCH_USERNAME'),
            'password': 'pass'
        }
        self.user = User.objects.create_user(**self.credentials)
        self.social_user = UserSocialAuth.objects.create(
            user=self.user,
            provider='twitch',
            extra_data=json.loads(config('EXAMPLE_SOCIAL_AUTH_EXTRA_DATA'))
        )

    def modify_session_oauth_login(self):
        session = self.client.session
        session.update({
            '_auth_user_backend': 'social_core.backends.twitch.TwitchOAuth2',
            'twitch_state': 'jVwxCUYygVDYAYOpqCdukzC8iqD0E9dy',
            'social_auth_last_login_backend': 'twitch',
        })
        session.save()

    def login(self):
        is_authenticated = self.client.login(**self.credentials)
        self.assertTrue(is_authenticated)

    def test_login_page_before_login(self):
        response = self.client.get('/auth/login/')
        self.assertEqual(response.status_code, 200)

    def test_login_template_before_login(self):
        response = self.client.get('/auth/login/')
        self.assertEqual(response.status_code, 200)
        self.assertNotContains(response, 'Logout')
        self.assertNotContains(response, 'Dashboard')

    def test_dashboard_redirect_not_logged(self):
        response = self.client.get('/stream/')
        self.assertRedirects(response, '/auth/login/?next=/stream/')

    def test_watch_stream_redirect_not_logged(self):
        response = self.client.get('/stream/watch/')
        self.assertRedirects(response, '/auth/login/?next=/stream/watch/')

    def test_login_page_after_login(self):
        self.login()
        response = self.client.get('/auth/login/')
        self.assertEqual(response.status_code, 200)

    def test_dashboard_after_login(self):
        self.login()
        self.modify_session_oauth_login()

        response = self.client.get('/stream/')
        self.assertEqual(response.status_code, 200)

    def test_dashboard_template_after_login(self):
        self.login()
        self.modify_session_oauth_login()
        response = self.client.get('/stream/')
        self.assertContains(response, 'Logout')
        self.assertContains(response, 'Dashboard')

    def test_logout_session(self):
        is_authenticated = self.client.login(**self.credentials)
        self.assertTrue(is_authenticated)

        response = self.client.get('/auth/logout/')
        self.assertRedirects(response, '/auth/login/')

        self.assertNotIn('_auth_user_id', self.client.session)

    def test_redirections_after_logout(self):
        is_authenticated = self.client.login(**self.credentials)
        self.assertTrue(is_authenticated)

        response = self.client.get('/auth/logout/')
        self.assertRedirects(response, '/auth/login/')

        response = self.client.get('/stream/')
        self.assertRedirects(response, '/auth/login/?next=/stream/')

        response = self.client.get('/stream/watch/')
        self.assertRedirects(response, '/auth/login/?next=/stream/watch/')

    def test_logout_social_user(self):
        is_authenticated = self.client.login(**self.credentials)
        self.assertTrue(is_authenticated)

        response = self.client.get('/auth/logout/')
        self.assertRedirects(response, '/auth/login/')

        social_user_count = UserSocialAuth\
            .objects.\
            filter(user=self.user).count()
        self.assertEqual(social_user_count, 0)
