#!/usr/bin/env bash

# Returns error
set -e

# Go to directory above (app directory)
cd "${0%/*}/.."

echo "Running tests"
flake8 . \
    --exclude .git,__pycache__,settings.py,old,build,dist,myenv,migrations \
    --ignore E501
./manage.py test