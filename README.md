# Favorite Streamer App

Link to the app: https://favorite-stream.herokuapp.com/

This application was developed using Django, the layout with Bootstrap(responsive),
and there are a few dynamic behaviors that are powered by JQuery and ES6.

* If the user tries to reach any page without being logged or without doing necessary steps for
some resource, the user is going to be redirected to the appropriate page.

* You can have a good understanding of how the application works by going looking
at the tests on oauth_login/tests.py and stream/tests.py

* To run the application with your own Twitch application () you can change the values on
test.env and rename the file to .env

* To test the application you can just rename test.env to .env and execute tests.

* There is also a pre-commit git-hook that performs code sanity checks before commiting
to ensure that commits that could break the build are avoided.





    



