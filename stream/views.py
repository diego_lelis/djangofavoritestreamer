from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views import View
from django.conf import settings

import json

from .helpers import TwitchAPI
from . import error_messages


@method_decorator(login_required, name='dispatch')
class Dashboard(View):
    template_name = 'stream/dashboard.html'

    def get(self, request):
        twitch_api = TwitchAPI(request)
        # twitch_api.refresh_token()
        twitch_username = twitch_api.get_twitch_username()
        context = {
            'twitch_username': twitch_username
        }
        return render(request, self.template_name, context)

    def render_page_with_error(self, request, error_message, status_code=200):
        return render(request, self.template_name, {
            'error_message': error_message,
        }, status=status_code)

    def render_get_streamer_error(self, request, streamer_req):
        error_req = json.loads(streamer_req.text)
        error_message = f"Code: {error_req.get('status')} | {error_req.get('error')}"
        if error_req.get('message'):
            error_message += f": {error_req.get('message')}"
        return self.render_page_with_error(request, error_message, error_req.get('status'))

    def post(self, request):
        streamer_name = request.POST['streamer_name']
        twitch_api = TwitchAPI(request)

        streamer_req = twitch_api.get_streamer_req(streamer_name)
        if streamer_req.status_code == 401:
            # Refresh token if requisition is denied as explained in
            # https://dev.twitch.tv/docs/authentication/#refresh-in-response-to-server-rejection-for-bad-authentication
            twitch_api.refresh_token()
            streamer_req = twitch_api.get_streamer_req(streamer_name)
        elif streamer_req.status_code != 200:
            return self.render_get_streamer_error(request, streamer_req)

        streamer_data = json.loads(streamer_req.text)['data']
        if not streamer_data:
            error_message = error_messages.INVALID_TWITCH_USERNAME
            return self.render_page_with_error(request, error_message)

        streamer_data = streamer_data[0]
        data = {'streamer_data': streamer_data}
        request.session['data'] = data

        return HttpResponseRedirect(reverse('stream:watch'))


@method_decorator(login_required, name='dispatch')
class Watch(View):
    template_name = 'stream/watch.html'

    @staticmethod
    def generate_session_data(streamer_data, twitch_api):
        data = {
            'streamer_data': streamer_data,
            'can_pub_sub': streamer_data.get('id') == twitch_api.get_user_id(),
            'api_data': {
                'client_key': settings.SOCIAL_AUTH_TWITCH_KEY,
                'events_url': settings.GET_EVENTS_API_TEMP.substitute(id=streamer_data.get('id'))
            }
        }
        # Passes necessary info to use pub_sub
        if data.get('can_pub_sub'):
            api_data_comp = {
                'access_token': twitch_api.get_access_token(),
                # 'expiration': twitch_api.get_expiration_time(),
                # 'refresh_token': twitch_api.get_refresh_token(),
                'ws_url': settings.TWITCH_WS_URL
            }
            data.get('api_data').update(api_data_comp)
        return data

    def get(self, request):
        data = request.session.get('data')
        if not data:
            return HttpResponseRedirect(reverse('stream:dashboard'))
        streamer_name = data.get('streamer_data').get('login')
        twitch_api = TwitchAPI(request)
        streamer_data = data.get('streamer_data')
        context = {
            'streamer_embed_url': settings.EMBED_PLAYER_URL_TEMP.substitute(streamer_name=streamer_name),
            'streamer_chat_url': settings.EMBED_CHAT_URL_TEMP.substitute(streamer_name=streamer_name),
            'can_pub_sub': streamer_data.get('id') == twitch_api.get_user_id(),
            'context_data': json.dumps(self.generate_session_data(
                streamer_data,
                twitch_api)
            )
        }
        return render(request, self.template_name, context=context)
