function generateActivityHTMLItem(data) {
  return `<li class="list-group-item" item-id=${data.message_id}>
      <span class="text-primary font-weight-bold">
        ${data.user_name || "anonymous"}
      </span>
        just
      <span class="text-info font-weight-bold">
        ${data.action}
      </span>
      on
      <span class="text-success font-weight-bold">
        ${data.channel}'s channel:
      </span>
      ${data.message}
    </li>`;
}


function addNewActivity(data) {
  itemData = {
    data,
    currentItems: currentActivMsg,
    listName: "activity-list",
    genItemFunc: generateActivityHTMLItem
  }
  addNewItem(itemData)
}


// generated id when there is lack of message_id or item_id
function generateItemId(messageAux) {
  return `${messageAux.user_id}${messageAux.channel_id}${messageAux.time}`
}

// Created using the documentation: https://dev.twitch.tv/docs/pubsub/
function createBitsV2Item(message) {
  let messageAux = message.data.message
  messageAux = JSON.parse(message.data.message)
  if (DEBUG) console.log("createBitsV2Item: ", typeof (messageAux));
  // console.log(messageAux);

  data = {
    item_id: messageAux.message_id,
    user_name: messageAux.data.user_name,
    action: messageAux.data.context,
    channel: messageAux.data.channel_name,
    message: messageAux.data.chat_message
  }
  addNewActivity(data)
}

// Created using the documentation: https://dev.twitch.tv/docs/pubsub/
function createBadgeItem(message) {
  let messageAux = message.data.message
  messageAux = JSON.parse(messageAux)
  if (DEBUG) console.log("createBadgeItem: ", messageAux);

  data = {
    item_id: generateItemId(messageAux) + messageAux.badge_tier,
    user_name: messageAux.user_name,
    action: `earned badge tier ${messageAux.badge_tier}`,
    channel: messageAux.channel_name,
    message: messageAux.chat_message
  }
  addNewActivity(data)
}

// Created using the documentation: https://dev.twitch.tv/docs/pubsub/
function createSubscribeItem(message) {
  let messageAux = message.data.data.message
  if (DEBUG) console.log("createSubscribeItem: ", messageAux);

  data = {
    item_id: generateItemId(messageAux),
    user_name: messageAux.user_name,
    action: messageAux.context,
    channel: messageAux.channel_name,
    message: messageAux.sub_message.message
  }
  addNewActivity(data)
}

// Created using the documentation: https://dev.twitch.tv/docs/pubsub/
function createCommerceItem(message) {
  let messageAux = message.data.message
  if (DEBUG) console.log("createCommerceItem: ", messageAux);

  data = {
    item_id: generateItemId(messageAux),
    user_name: messageAux.user_name,
    action: `bought
    <img class="rounded-circle" 
    src="${messageAux.item_image_url}" 
    alt="${messageAux.item_description}" width="20">`,
    channel: messageAux.channel_name,
    message: messageAux.purchase_message.message
  }
  addNewActivity(data)
}

// Tested with real data
function createWhisperItem(message) {
  console.log(message);

  let messageAux = message.data.message
  messageAux = JSON.parse(messageAux).data_object
  if (DEBUG) console.log("createWhisperItem: ", typeof (messageAux));

  data = {
    item_id: messageAux.nonce,
    user_name: messageAux.tags.login,
    action: `whispered`,
    channel: messageAux.recipient.username,
    message: messageAux.body
  }
  addNewActivity(data)
}


function chooseAndAddActvItem(message) {
  // console.log(message);

  if (typeof (message.data.topic) !== 'undefined') {
    if (message.data.topic.startsWith('channel-bits-events-v2')) {
      if (DEBUG) console.log('bits ', 'detected');
      createBitsV2Item(message)
    } else if (message.data.topic.startsWith('channel-bits-badge-unlocks.')) {
      if (DEBUG) console.log('badge ', 'detected');
      createBadgeItem(message)
    } else if (message.data.topic.startsWith('channel-commerce-events-v1.')) {
      if (DEBUG) console.log('commerce ', 'detected');
      createCommerceItem(message)
    } else if (message.data.topic.startsWith('whispers.')) {
      if (DEBUG) console.log('whispers ', 'detected');
      createWhisperItem(message)
    }
  } else if (typeof (message.data.data) !== 'undefined') {
    if (message.data.data.topic.startsWith('channel-subscribe-events-v1.')) {
      if (DEBUG) console.log('subscribe ', 'detected');
      createSubscribeItem(message)
    }
  }
}



/// Tests

function testBitsV2Insert() {
  message = {
    type: "MESSAGE",
    data: {
      topic: "channel-bits-events-v2.46024993",
      message: '{"data":{"user_name":"jwp","channel_name":"bontakun","user_id":"95546976","channel_id":"46024993","time":"2017-02-09T13:23:58.168Z","chat_message":"cheer10000 New badge hype!","bits_used":10000,"total_bits_used":25000,"context":"cheer","badge_entitlement":{"new_version":25000,"previous_version":10000}},"version":"1.0","message_type":"bits_event","message_id":"8145728a4-35f0-4cf7-9dc0-f2ef24de1eb6","is_anonymous":true}'
    }
  };
  createBitsV2Item(message);
}

function testBitsBadgeInsert() {
  message = {
    "type": "MESSAGE",
    "data": {
      "topic": "channel-bits-badge-unlocks.401394874",
      "message": `{
           \"user_id\":\"232889822\",\"user_name\":\"willowolf\",\"channel_id\":\"401394874\",\"channel_name\":\"fun_test12345\",\"badge_tier\":1000,\"chat_message\":\"this should be received by the public pubsub listener\",\"time\":\"2020-12-06T00:01:43.71253159Z\"
          }`
    }
  };
  createBadgeItem(message);
}


function testSubscribeInsert() {
  message = {
    type: "MESSAGE",
    data: {
      "type": "MESSAGE",
      "data": {
        "topic": "channel-subscribe-events-v1.44322889",
        "message": {
          "user_name": "dallas",
          "display_name": "dallas",
          "channel_name": "twitch",
          "user_id": "44322889",
          "channel_id": "12826",
          "time": "2015-12-19T16:39:57-08:00",
          "sub_plan": "Prime" / "1000" / "2000" / "3000",
          "sub_plan_name": "Channel Subscription (mr_woodchuck)",
          "cumulative-months": 9,
          "streak-months": 3,
          "context": "sub",
          "sub_message": {
            "message": "A Twitch baby is born! KappaHD",
            "emotes": [{
              "start": 23,
              "end": 7,
              "id": 2867
            }]
          }
        }
      }
    }
  };
  createSubscribeItem(message);
}

function testCommerceInsert() {
  message = {
    "type": "MESSAGE",
    "data": {
      "topic": "channel-commerce-events-v1.44322889",
      "message": {
        "user_name": "dallas",
        "display_name": "dallas",
        "channel_name": "twitch",
        "user_id": "44322889",
        "channel_id": "12826",
        "time": "2015-12-19T16:39:57-08:01",
        "item_image_url": "data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==",
        "item_description": "This is a friendly description!",
        "supports_channel": true,
        "purchase_message": {
          "message": "A Twitch game is born! Kappa",
          "emotes": [{
            "start": 23,
            "end": 7,
            "id": 2867
          }]
        }
      }
    }
  };
  createCommerceItem(message);
}

// Data used modified based on real messages
function testWhiperInsert() {
  message = {
    "type": "MESSAGE",
    "data": {
      "topic": "whispers.44322889",
      "message": `{
        "data_object": {
          "id": 41,
          "thread_id": "129454141_44322889",
          "body": "hello",
          "sent_ts": 1479160009,
          "from_id": 44322889,
          "tags": {
            "login": "dallas",
            "display_name": "dallas",
            "color": "#8A2BE2",
            "emotes": [],
            "badges": [{
              "id": "staff",
              "version": "1"
            }]
          },
          "recipient": {
            "id": 129454141,
            "username": "dallasnchains",
            "display_name": "dallasnchains",
            "color": "",
            "badges": []
          },
          "nonce": "6GVBTfBXNj7d71BULYKjpiKapegDI1"
        }
      }`
    }
  };
  createWhisperItem(message);
}


function chooseAndAddActvItemTest() {
  messages = [{
      type: "MESSAGE",
      data: {
        topic: "channel-bits-events-v2.46024993",
        message: '{"data":{"user_name":"jwp","channel_name":"bontakun","user_id":"95546976","channel_id":"46024993","time":"2017-02-09T13:23:58.168Z","chat_message":"cheer10000 New badge hype!","bits_used":10000,"total_bits_used":25000,"context":"cheer","badge_entitlement":{"new_version":25000,"previous_version":10000}},"version":"1.0","message_type":"bits_event","message_id":"8145728a4-35f0-4cf7-9dc0-f2ef24de1eb6","is_anonymous":true}'
      }
    },
    {
      "type": "MESSAGE",
      "data": {
        "topic": "channel-bits-badge-unlocks.401394874",
        "message": `{
               \"user_id\":\"232889822\",\"user_name\":\"willowolf\",\"channel_id\":\"401394874\",\"channel_name\":\"fun_test12345\",\"badge_tier\":1000,\"chat_message\":\"this should be received by the public pubsub listener\",\"time\":\"2020-12-06T00:01:43.71253159Z\"
              }`
      }
    },
    {
      type: "MESSAGE",
      data: {
        "type": "MESSAGE",
        "data": {
          "topic": "channel-subscribe-events-v1.44322889",
          "message": {
            "user_name": "dallas",
            "display_name": "dallas",
            "channel_name": "twitch",
            "user_id": "44322889",
            "channel_id": "12826",
            "time": "2015-12-19T16:39:57-08:00",
            "sub_plan": "Prime" / "1000" / "2000" / "3000",
            "sub_plan_name": "Channel Subscription (mr_woodchuck)",
            "cumulative-months": 9,
            "streak-months": 3,
            "context": "sub",
            "sub_message": {
              "message": "A Twitch baby is born! KappaHD",
              "emotes": [{
                "start": 23,
                "end": 7,
                "id": 2867
              }]
            }
          }
        }
      }
    },
    {
      "type": "MESSAGE",
      "data": {
        "topic": "channel-commerce-events-v1.44322889",
        "message": {
          "user_name": "dallas",
          "display_name": "dallas",
          "channel_name": "twitch",
          "user_id": "44322889",
          "channel_id": "12826",
          "time": "2015-12-19T16:39:57-08:01",
          "item_image_url": "data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==",
          "item_description": "This is a friendly description!",
          "supports_channel": true,
          "purchase_message": {
            "message": "A Twitch game is born! Kappa",
            "emotes": [{
              "start": 23,
              "end": 7,
              "id": 2867
            }]
          }
        }
      }
    },
    {
      "type": "MESSAGE",
      "data": {
        "topic": "whispers.44322889",
        "message": `{
          "data_object": {
            "id": 41,
            "thread_id": "129454141_44322889",
            "body": "hello",
            "sent_ts": 1479160009,
            "from_id": 44322889,
            "tags": {
              "login": "dallas",
              "display_name": "dallas",
              "color": "#8A2BE2",
              "emotes": [],
              "badges": [{
                "id": "staff",
                "version": "1"
              }]
            },
            "recipient": {
              "id": 129454141,
              "username": "dallasnchains",
              "display_name": "dallasnchains",
              "color": "",
              "badges": []
            },
            "nonce": "6GVBTfBXNj7d71BULYKjpiKapegDI1"
          }
        }`
      }
    }
  ]
  for (message of messages) {
    chooseAndAddActvItem(message)
  }
}