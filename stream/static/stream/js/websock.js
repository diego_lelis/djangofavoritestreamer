'strict mode'

/// Websockets

// Source: https://www.thepolyglotdeveloper.com/2015/03/create-a-random-nonce-string-using-javascript/
function nonce(length) {
    var text = "";
    var possible =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < length; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
}

function heartbeat() {
    message = {
        type: "PING"
    };
    if (DEBUG)
        console.log("SENT: " + JSON.stringify(message) + "\n");
    ws.send(JSON.stringify(message));
}

function listen(topic, contextData) {
    message = {
        type: "LISTEN",
        nonce: nonce(15),
        data: {
            topics: [topic],
            auth_token: contextData.api_data.access_token
        }
    };
    if (DEBUG)
        console.log("SENT: " + JSON.stringify(message) + "\n");
    ws.send(JSON.stringify(message));
}

function listenAllTopics(contextData) {
    // Starts to listen to all the available topics for the user
    listen(
        `channel-bits-events-v2.${contextData.streamer_data.id}`,
        contextData
    );
    listen(
        `channel-bits-badge-unlocks.${contextData.streamer_data.id}`,
        contextData
    );
    listen(
        `channel-subscribe-events-v1.${contextData.streamer_data.id}`,
        contextData
    );
    listen(
        `channel-commerce-events-v1.${contextData.streamer_data.id}`,
        contextData
    );
    listen(
        `whispers.${contextData.streamer_data.id}`,
        contextData);
}

function connect(contextData) {
    var heartbeatInterval = 1000 * 60; //ms between PING's
    var reconnectInterval = 1000 * 3; //ms to wait before reconnect
    var heartbeatHandle;

    ws = new WebSocket(contextData.api_data.ws_url);

    ws.onopen = function (event) {
        if (DEBUG) console.log("INFO: Socket Opened\n");
        heartbeat();
        heartbeatHandle = setInterval(heartbeat, heartbeatInterval);
        listenAllTopics(contextData)
    };

    ws.onerror = function (error) {
        if (DEBUG) console.log("ERR:  " + JSON.stringify(error) + "\n");
    };

    ws.onmessage = function (event) {
        message = JSON.parse(event.data);
        if (DEBUG) console.log("RECV: " + JSON.stringify(message) + "\n");
        if (message.type == "RECONNECT") {
            if (DEBUG) console.log("INFO: Reconnecting...\n");
            setTimeout(connect, reconnectInterval);
        } else if (message.type == "MESSAGE") {
            if (DEBUG) console.log("RECV-MSG: ", JSON.parse(message.data.message));
            chooseAndAddActvItem(message);
        }
    };

    ws.onclose = function () {
        if (DEBUG) console.log("INFO: Socket Closed\n");
        clearInterval(heartbeatHandle);
        if (DEBUG) console.log("INFO: Reconnecting...\n");
        setTimeout(connect, reconnectInterval);
    };
}

/// Websockets