function addNewItem({
    data,
    currentItems,
    listName,
    genItemFunc
}) {
    if (currentItems.has(STARTMESSAGEHASH)) {
        $(`#${listName} li`)
            .last()
            .remove();
        currentItems.delete(STARTMESSAGEHASH);
    }
    if (!currentItems.has(data.item_id)) {
        // Comment to see printing the 10 elements
        while ($(`#${listName} li`).length + 1 >= 10) {
            last = $(`#${listName} li`).last();
            messageId = last.attr("item-id");
            currentItems.delete(messageId);
            last.remove();
        }
        $(`#${listName}`).prepend(genItemFunc(data));
        currentItems.add(data.item_id);
    }
}



$(document).ready(function () {
    if (TESTING) {
        console.log("Test wuhu")
         chooseAndAddActvItemTest()
//         testBitsV2Insert()
//         testBitsBadgeInsert()
//         testSubscribeInsert()
//         testCommerceInsert()
//         testWhiperInsert()
         testAddEvents()
    }


    if (DEBUG) console.log("contextData: ", contextData); // data passed to be used on JS

    if (contextData.can_pub_sub) {
        connect_func = connect;
        connect_func(contextData);
    }

    getEventsAsync(contextData)

})