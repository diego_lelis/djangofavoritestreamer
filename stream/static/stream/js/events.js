'strict mode'

function generateEventHTMLItem(data) {
    return `
    <li
      class="list-group-item 
      list-group-item-action 
      flex-column 
      align-items-start"
      item-id=${data.event_id}
      >
      <div class="d-flex w-100 text-right">
        <small class="text-muted text-right">${data.time}</small>
      </div>
      <p class="mb-1">${data.title}.</p>
      <small class="text-muted">${data.desc}</small>
    </li>`;
}

function addNewEvent(data) {
    let itemData = {
        data,
        currentItems: currentEvents,
        listName: "event-list",
        genItemFunc: generateEventHTMLItem
    }
    addNewItem(itemData)
}

function testAddEvents() {
    for (i = 0; i < 12; i++) {
        let data = {
            item_id: `${i}`,
            title: `Teste ${i}`,
            desc: `Teste Desc ${i}`,
            time: Date.now()
        }
        addNewEvent(data)
        if (DEBUG) console.log('Event try: ', i);

    }
}

function getEventsAsync(contextData) {
    const client = new XMLHttpRequest();
    const url = contextData.api_data.events_url;
    client.onreadystatechange = function () {
        if (this.readyState == 4) { // Treat token invalid
            if (this.status == 200) {
                if (DEBUG) console.log(typeof (client.responseText))
                processEvents(client.responseText)
            } else {
                let data = {
                    item_id: 'error',
                    title: 'Error',
                    desc: 'An error occurred while loading the events, please reload the page.',
                    time: ''
                }
            }
        }
    }
    client.open("GET", url, true)
    client.setRequestHeader('Client-ID', contextData.api_data.client_key);
    client.send()
}

function processEvents(responseText) {
    let events = JSON.parse(responseText).events;
    events = events.slice(0, Math.min(10, events.length))
    if (events.length > 0) {
        events.map((event) => {
            let time = Date.parse(event.start_time)
            time = new Date(time)
                .toLocaleString("en-US")
            if (DEBUG) console.log(event);
            let data = {
                item_id: event._id,
                title: event.title,
                desc: event.description,
                time
            }
            addNewEvent(data)

        })
    } else {
        let data = {
            item_id: 'no-events',
            title: 'No events',
            desc: 'this user has no events planned',
            time: ''
        }
        addNewEvent(data)
    }
}