import jwt
from social_django.utils import load_strategy
from django.conf import settings
import requests
import json


class TwitchAPI:
    def __init__(self, request):
        self.twitch_login = request.user.social_auth.get(provider='twitch')
        id_token = self.twitch_login.extra_data.get('id_token')
        self.id_token_decoded = jwt.decode(id_token, algorithms='RS256', verify=False)
        # Verify=False because of this issue: https://github.com/jpadilla/pyjwt/issues/359

    def add_data_request(self, request):
        """It can only be runned after it is confirmed that the twitch username exists"""
        pass

    def get_streamer_req(self, streamer_name):
        get_user_url = settings.GET_USER_INFO_API_TEMP.substitute(streamer_name=streamer_name)
        # print('get_user_url : ', get_user_url)
        headers = json.loads(settings.AUTH_HEADER_GENERATION.substitute(
            token=self.get_access_token()
        ))
        req = requests.get(get_user_url, headers=headers)
        # print('req.json():', req.json())
        return req

    def get_auth_time(self):
        return self.twitch_login.extra_data.get('auth_time')

    def get_expiration_in(self):
        return self.twitch_login.extra_data.get('expires_in')

    def get_expiration_time(self):
        """"
        Tested to see if after exp time the api still works and it works, sum of values is the validation time to use
        """
        auth_time = self.get_auth_time()
        expires_in = self.get_expiration_in()
        return auth_time + expires_in

    def get_twitch_username(self):
        return self.id_token_decoded.get('preferred_username')

    def get_access_token(self):
        return self.twitch_login.extra_data.get('access_token')

    def get_refresh_token(self):
        return self.twitch_login.extra_data.get('refresh_token')

    def get_user_id(self):
        return self.id_token_decoded.get('sub')

    def refresh_token(self):
        self.twitch_login.refresh_token(load_strategy())
