from django.test import TestCase
from django.contrib.auth.models import User
from social_django.models import UserSocialAuth
import json
from decouple import config
from django.conf import settings
import responses

from . import error_messages


class StreamViewTest(TestCase):
    def setUp(self):
        self.credentials = {
            'username': config('TWITCH_USERNAME'),
            'password': 'pass'
        }
        self.user = User.objects.create_user(**self.credentials)
        self.social_user = UserSocialAuth.objects.create(
            user=self.user,
            provider='twitch',
            extra_data=json.loads(config('EXAMPLE_SOCIAL_AUTH_EXTRA_DATA'))
            # username on the encrypted id_token needs to be equal to the username on credentials
        )
        self.login()

    def login(self):
        is_authenticated = self.client.login(**self.credentials)
        self.assertTrue(is_authenticated)

    def test_dashboard_template_username(self):
        response = self.client.get('/stream/')
        self.assertContains(response, self.credentials['username'])

    def test_watch_stream_before_user_is_set(self):
        response = self.client.get('/stream/watch/')
        self.assertRedirects(response, '/stream/')  # Goes back to dashboard because proper session data was not added.

    def create_mock_resp_invalid_twitch_user(self):
        responses.add(responses.GET,
                      settings.GET_USER_INFO_API_TEMP.substitute(streamer_name=self.streamer_name),
                      json={'data': []},
                      headers=json.loads(settings.AUTH_HEADER_GENERATION.substitute(
                          token=self.social_user.extra_data.get('access_token')))
                      )

    def create_mock_resp_valid_user_no_pub_sub(self):
        responses.add(responses.GET,
                      settings.GET_USER_INFO_API_TEMP.substitute(streamer_name=self.streamer_name),
                      json={'data': [
                          {
                              'id': '39298218',
                              'login': 'dakotaz',
                              'display_name': 'dakotaz',
                              'type': '',
                              'broadcaster_type': 'partner',
                              'description': 'no gimmicks, just skill.',
                              'profile_image_url': 'https://static-cdn.jtvnw.net/jtv_user_pictures/473aea0f-a724-498f-b7f1-e344f806ba8a-profile_image-300x300.png',
                              'offline_image_url': 'https://static-cdn.jtvnw.net/jtv_user_pictures/565c4f30-3a64-48de-a448-e11a72779a1a-channel_offline_image-1920x1080.png',
                              'view_count': 82341503
                          }
                      ]
                      },
                      headers=json.loads(settings.AUTH_HEADER_GENERATION.substitute(
                          token=self.social_user.extra_data.get('access_token')))
                      )

    def create_mock_resp_valid_user_with_pub_sub(self):
        responses.add(responses.GET,
                      settings.GET_USER_INFO_API_TEMP.substitute(streamer_name=self.streamer_name),
                      json={
                          'data':
                              [
                                  {
                                      'id': '111111111',
                                      'login': 'twitchusertest',
                                      'display_name': 'twitchusertest',
                                      'type': '',
                                      'broadcaster_type': 'test',
                                      'description': 'test',
                                      'profile_image_url':
                                          'data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==',
                                      'offline_image_url':
                                          'data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==',
                                      'view_count': 25
                                  }
                              ]
                      },
                      headers=json.loads(
                          settings.AUTH_HEADER_GENERATION.substitute(
                              token=self.social_user.extra_data.get('access_token')
                          ))
                      )

    @responses.activate
    def test_dashboard_invalid_username(self):
        self.streamer_name = 'aew'
        self.create_mock_resp_invalid_twitch_user()
        response = self.client.post('/stream/', {'streamer_name': self.streamer_name})
        self.assertContains(response, error_messages.INVALID_TWITCH_USERNAME)

    def helper_dash_valid_username(self):
        self.streamer_name = 'dakotaz'
        self.create_mock_resp_valid_user_no_pub_sub()
        response = self.client.post('/stream/', {'streamer_name': self.streamer_name})
        return response

    @responses.activate
    def test_dashboard_valid_username(self):
        response = self.helper_dash_valid_username()
        self.assertRedirects(response, '/stream/watch/')

    def helper_dash_same_log_user(self):
        self.streamer_name = 'twitchusertest'
        self.create_mock_resp_valid_user_with_pub_sub()
        response = self.client.post('/stream/', {'streamer_name': self.streamer_name})
        return response

    @responses.activate
    def test_dashboard_same_logged_user(self):
        response = self.helper_dash_same_log_user()
        self.assertRedirects(response, '/stream/watch/')

    @responses.activate
    def test_watch_stream_no_pub_sub(self):
        self.helper_dash_valid_username()  # Modifies the session with the necessary values
        response = self.client.get('/stream/watch/')
        self.assertEqual(response.status_code, 200)

    @responses.activate
    def test_watch_stream_template_no_pub_sub(self):
        self.helper_dash_valid_username()  # Modifies the session with the necessary values
        response = self.client.get('/stream/watch/')
        self.assertContains(response, self.streamer_name)

    @responses.activate
    def test_watch_stream_with_pub_sub(self):
        self.helper_dash_same_log_user()  # Modifies the session with the necessary values
        response = self.client.get('/stream/watch/')
        self.assertEqual(response.status_code, 200)

    @responses.activate
    def test_watch_stream_template_with_pub_sub(self):
        self.helper_dash_same_log_user()  # Modifies the session with the necessary values
        response = self.client.get('/stream/watch/')
        self.assertContains(response, self.streamer_name)
        self.assertContains(response, 'Last Chat Events')
