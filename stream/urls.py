from django.conf.urls import url

from . import views

app_name = 'stream'

urlpatterns = [
    url(r'^$', views.Dashboard.as_view(), name='dashboard'),
    url(r'^watch/', views.Watch.as_view(), name='watch'),
]
